# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/notification/distributed_notification_service/notification.gni")
import("//build/ohos.gni")
import("//build/test.gni")

module_output_path = "${component_name}/unittest"

ohos_unittest("ans_unit_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "//commonlibrary/c_utils/base/include",
    "//utils/system/safwk/native/include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  defines = []

  sources = [
    "${services_path}/ans/src/advanced_notification_service.cpp",
    "${services_path}/ans/src/advanced_notification_service_ability.cpp",
    "${services_path}/ans/src/event_report.cpp",
    "${services_path}/ans/src/notification_preferences.cpp",
    "${services_path}/ans/src/notification_preferences_database.cpp",
    "${services_path}/ans/src/notification_preferences_info.cpp",
    "${services_path}/ans/src/notification_slot_filter.cpp",
    "${services_path}/ans/src/notification_subscriber_manager.cpp",
    "${services_path}/ans/src/permission_filter.cpp",
    "${services_path}/ans/src/reminder_data_manager.cpp",
    "${services_path}/ans/src/reminder_event_manager.cpp",
    "${services_path}/ans/src/reminder_timer_info.cpp",
    "${services_path}/ans/src/system_event_observer.cpp",
    "advanced_notification_service_ability_test.cpp",
    "advanced_notification_service_test.cpp",
    "bundle_manager_helper_test.cpp",
    "mock/blob.cpp",
    "mock/distributed_kv_data_manager.cpp",
    "mock/mock_access_token_helper.cpp",
    "mock/mock_accesstoken_kit.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_event_handler.cpp",
    "mock/mock_ipc.cpp",
    "mock/mock_single_kv_store.cpp",
    "notification_hisysevent_test.cpp",
    "notification_preferences_database_test.cpp",
    "notification_preferences_test.cpp",
    "notification_slot_filter_test.cpp",
    "notification_subscriber_manager_test.cpp",
    "permission_filter_test.cpp",
    "reminder_data_manager_test.cpp",
    "system_event_observer_test.cpp",
  ]

  configs = [ "//commonlibrary/c_utils/base:utils_config" ]

  deps = [
    "${core_path}:ans_core",
    "${frameworks_module_ans_path}:ans_innerkits",
    "//foundation/arkui/ace_engine/interfaces/inner_api/ui_service_manager:ui_service_mgr",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hitrace_native:hitrace_meter",
    "hitrace_native:libhitracechain",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "multimedia_image_framework:image_native",
    "multimedia_player_framework:media_client",
    "os_account:os_account_innerkits",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  if (device_usage) {
    external_deps += [ "device_usage_statistics:usagestatsinner" ]
    defines += [ "DEVICE_USAGE_STATISTICS_ENABLE" ]
  }

  cflags = []
  if (hisysevent_usage) {
    external_deps += [ "hisysevent_native:libhisysevent" ]
    cflags += [ "-DHAS_HISYSEVENT_PART" ]
  }

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

group("unittest") {
  testonly = true
  deps = []

  deps += [ ":ans_unit_test" ]

  deps += [ "access_token_helper_test:unittest" ]
}
