/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "napi_subscribe.h"

#include "ans_inner_errors.h"
#include "subscribe.h"
#include "unsubscribe.h"

namespace OHOS {
namespace NotificationNapi {
napi_value NapiSubscribe(napi_env env, napi_callback_info info)
{
    ANS_LOGI("enter");
    napi_ref callback = nullptr;
    SubscriberInstance *objectInfo = nullptr;
    NotificationSubscribeInfo subscriberInfo;
    if (ParseParameters(env, info, subscriberInfo, objectInfo, callback) == nullptr) {
        if (objectInfo) {
            delete objectInfo;
            objectInfo = nullptr;
        }
        Common::NapiThrow(env, ERROR_PARAM_INVALID);
        return Common::NapiGetUndefined(env);
    }

    AsyncCallbackInfoSubscribe *asynccallbackinfo = new (std::nothrow) AsyncCallbackInfoSubscribe {
        .env = env, .asyncWork = nullptr, .objectInfo = objectInfo, .subscriberInfo = subscriberInfo
    };
    if (!asynccallbackinfo) {
        if (objectInfo) {
            delete objectInfo;
            objectInfo = nullptr;
        }
        return Common::JSParaError(env, callback);
    }
    napi_value promise = nullptr;
    Common::PaddingCallbackPromiseInfo(env, callback, asynccallbackinfo->info, promise);

    napi_value resourceName = nullptr;
    napi_create_string_latin1(env, "subscribeNotification", NAPI_AUTO_LENGTH, &resourceName);
    // Asynchronous function call
    napi_create_async_work(env,
        nullptr,
        resourceName,
        [](napi_env env, void *data) {
            ANS_LOGI("Subscribe napi_create_async_work start");
            if (!data) {
                ANS_LOGE("Invalid asynccallbackinfo!");
                return;
            }
            auto asynccallbackinfo = reinterpret_cast<AsyncCallbackInfoSubscribe *>(data);
            if (asynccallbackinfo->subscriberInfo.hasSubscribeInfo) {
                ANS_LOGI("Subscribe with NotificationSubscribeInfo");
                OHOS::Notification::NotificationSubscribeInfo subscribeInfo;
                subscribeInfo.AddAppNames(asynccallbackinfo->subscriberInfo.bundleNames);
                subscribeInfo.AddAppUserId(asynccallbackinfo->subscriberInfo.userId);
                asynccallbackinfo->info.errorCode =
                    NotificationHelper::SubscribeNotification(*(asynccallbackinfo->objectInfo), subscribeInfo);
            } else {
                asynccallbackinfo->info.errorCode =
                    NotificationHelper::SubscribeNotification(*(asynccallbackinfo->objectInfo));
            }
        },
        [](napi_env env, napi_status status, void *data) {
            ANS_LOGI("Subscribe napi_create_async_work end");
            if (!data) {
                ANS_LOGE("Invalid asynccallbackinfo!");
                return;
            }
            auto asynccallbackinfo = reinterpret_cast<AsyncCallbackInfoSubscribe *>(data);
            Common::CreateReturnValue(env, asynccallbackinfo->info, Common::NapiGetNull(env));

            if (asynccallbackinfo->info.callback != nullptr) {
                napi_delete_reference(env, asynccallbackinfo->info.callback);
            }
            napi_delete_async_work(env, asynccallbackinfo->asyncWork);

            delete asynccallbackinfo;
            asynccallbackinfo = nullptr;
        },
        (void *)asynccallbackinfo,
        &asynccallbackinfo->asyncWork);

    NAPI_CALL(env, napi_queue_async_work(env, asynccallbackinfo->asyncWork));
    if (asynccallbackinfo->info.isCallback) {
        return Common::NapiGetNull(env);
    } else {
        return promise;
    }
}

napi_value NapiUnsubscribe(napi_env env, napi_callback_info info)
{
    ANS_LOGI("Unsubscribe start");
    ParametersInfoUnsubscribe paras;
    if (ParseParameters(env, info, paras) == nullptr) {
        Common::NapiThrow(env, ERROR_PARAM_INVALID);
        return Common::NapiGetUndefined(env);
    }

    AsyncCallbackInfoUnsubscribe *asynccallbackinfo = new (std::nothrow)
        AsyncCallbackInfoUnsubscribe {.env = env, .asyncWork = nullptr, .objectInfo = paras.objectInfo};
    if (!asynccallbackinfo) {
        return Common::JSParaError(env, paras.callback);
    }
    napi_value promise = nullptr;
    Common::PaddingCallbackPromiseInfo(env, paras.callback, asynccallbackinfo->info, promise);

    napi_value resourceName = nullptr;
    napi_create_string_latin1(env, "unsubscribe", NAPI_AUTO_LENGTH, &resourceName);

    // Asynchronous function call
    napi_create_async_work(env,
        nullptr,
        resourceName,
        [](napi_env env, void *data) {
            ANS_LOGI("Unsubscribe napi_create_async_work start");
            auto asynccallbackinfo = reinterpret_cast<AsyncCallbackInfoUnsubscribe *>(data);

            if (asynccallbackinfo->objectInfo == nullptr) {
                ANS_LOGE("invalid object info");
                asynccallbackinfo->info.errorCode = ERR_ANS_INVALID_PARAM;
                return;
            }

            bool ret = AddDeletingSubscriber(asynccallbackinfo->objectInfo);
            if (ret) {
                asynccallbackinfo->info.errorCode =
                    NotificationHelper::UnSubscribeNotification(*(asynccallbackinfo->objectInfo));
                if (asynccallbackinfo->info.errorCode != ERR_OK) {
                    DelDeletingSubscriber(asynccallbackinfo->objectInfo);
                }
            } else {
                asynccallbackinfo->info.errorCode = ERR_ANS_SUBSCRIBER_IS_DELETING;
            }
        },
        [](napi_env env, napi_status status, void *data) {
            ANS_LOGI("Unsubscribe napi_create_async_work end");
            AsyncCallbackInfoUnsubscribe *asynccallbackinfo = static_cast<AsyncCallbackInfoUnsubscribe *>(data);
            if (asynccallbackinfo) {
                Common::CreateReturnValue(env, asynccallbackinfo->info, Common::NapiGetNull(env));
                if (asynccallbackinfo->info.callback != nullptr) {
                    napi_delete_reference(env, asynccallbackinfo->info.callback);
                }
                napi_delete_async_work(env, asynccallbackinfo->asyncWork);
                delete asynccallbackinfo;
                asynccallbackinfo = nullptr;
            }
        },
        (void *)asynccallbackinfo,
        &asynccallbackinfo->asyncWork);

    NAPI_CALL(env, napi_queue_async_work(env, asynccallbackinfo->asyncWork));
    if (asynccallbackinfo->info.isCallback) {
        return Common::NapiGetNull(env);
    } else {
        return promise;
    }
}
}  // namespace NotificationNapi
}  // namespace OHOS